<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Este metodo cria automaticamente todas as rotas de login/registro e etc
Auth::routes();

Route::get('/', function () {
    return redirect('/home');
});

Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

//Rotas dentro desse grupo usam o middleware auth
//Isso quer dizer que só usuários logados tem permissão de acesso
//Quem não estiver logado é redirecionado para a página de login
Route::group(['middleware' => ['auth']], function () 
{
    //Controllers resource já possuem suas rotas fixas
    //https://laravel.com/docs/5.8/controllers#resource-controllers
	Route::resource('user', 'UserController');
	Route::resource('news', 'NewsController');
    Route::resource('contact', 'ContactController');
});
