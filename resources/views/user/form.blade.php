@extends('layouts.admin')
@section('title', $form_title)

@section('toolbar')
  <button class="btn btn-sm btn-primary" id="salvar"><i data-feather="save"></i> Salvar</button>&nbsp;
  <a href="{{route('user.index')}}" class="btn btn-sm btn-primary"><i data-feather="arrow-left"></i> Voltar</a>
@endsection

@section('content')
<div class="col-md-12">
    <form action="{{$form_route}}" method="POST" name="user_form">
      @method($form_method)
      @csrf
      <div class="row">
        <div class="col-md-12 mb-3">
          <label for="firstName">Nome</label>
          <input name="name" type="text" class="form-control" placeholder="" value="{{$user->name ?? ''}}" required>
          <div class="invalid-feedback">
            Valid first name is required.
          </div>
        </div>        
      </div> 

      <div class="mb-3">
        <label for="email">Email </label>
        <input name="email" type="email" class="form-control" placeholder="you@example.com" value="{{$user->email ?? ''}}">
        <div class="invalid-feedback">
          Please enter a valid email address for shipping updates.
        </div>
      </div>

      <div class="mb-3">
        <label for="email">Senha </label>
        <input name="password" type="password" class="form-control" value="">
        <div class="invalid-feedback">
          Please enter a valid email address for shipping updates.
        </div>
      </div>

      <div class="mb-3">
        <label for="email">Confirmar Senha </label>
        <input name="password_confirmation" type="password" class="form-control" value="">
        <div class="invalid-feedback">
          Please enter a valid email address for shipping updates.
        </div>
      </div>

      <input type="submit" style="display: none;" />

    </form>
  </div>
</div>
@endsection

@push('scripts')
  <script type="text/javascript">
    $(document).ready(function()
    {
      $('#salvar').click(function()
      {        
        $('form[name=user_form]').submit();        
      });      
      
    });

  </script>
@endpush