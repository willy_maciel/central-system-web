@extends('layouts.admin')
@section('title', 'Exibir Usuário')

@section('toolbar')
  <a href="{{route('user.index')}}" class="btn btn-sm btn-primary" id="salvar">Voltar</a>
@endsection

@section('content')
<div class="col-md-12">
      <div class="row">
        <div class="col-md-12 mb-3">
          <label for="firstName">Nome</label>
          <input name="name" type="text" class="form-control" placeholder="" value="{{$user->name ?? ''}}" readonly="readonly">
        </div>        
      </div> 

      <div class="mb-3">
        <label for="email">Email </label>
        <input name="email" type="email" class="form-control" value="{{$user->email ?? ''}}" readonly="readonly">
      </div>

      <div class="mb-3">
        <label for="email">Criado em </label>
        <input name="email" type="text" class="form-control" value="{{$user->created_at ?? ''}}" readonly="readonly">
      </div>

      <div class="mb-3">
        <label for="email">Modificado em </label>
        <input name="email" type="text" class="form-control" value="{{$user->updated_at ?? ''}}" readonly="readonly">      
      </div>

  </div>
</div>
@endsection