@extends('layouts.admin')
@section('title', 'Listar usuários')

@section('toolbar')
	<a href="{{route('user.create')}}" class="btn btn-sm btn-primary"><i data-feather="plus-circle"></i> Novo</a>&nbsp;
	<button class="btn btn-sm btn-danger" id="excluir"><i data-feather="trash"></i> Excluir selecionados</button>
@endsection

@section('content')
	@if(!isset($users) || count($users) <= 0)
		@include('shared.no_results')
	@else
	<form action="{{url('user/destroy')}}" method="POST" id="users">
		@method('DELETE')
		@csrf
		<div class="table-responsive">	
			<table class="table table-striped table-sm">
			  <thead>
			    <tr>
			      <th><input type="checkbox" id="toggle_all"/></th>
			      <th>#</th>
			      <th>Nome</th>
			      <th>E-mail</th>
			      <th>Criado em</th>
			      <th>Editado em</th>
			      <th>Ações</th>
			    </tr>
			  </thead>
			  <tbody>
			  @foreach($users as $user)
			    <tr>
			      <td><input type="checkbox" name="ids[]" class="ids" value="{{$user->id}}" /></td>
			      <td>{{$user->id}}</td>
			      <td>{{$user->name}}</td>
			      <td>{{$user->email}}</td>
			      <td>{{$user->created_at->format('d/m/Y \\a\\s H:i:s')}}</td>
			      <td>{{$user->updated_at->format('d/m/Y \\a\\s H:i:s')}}</td>
			      <td>
			      	<a class="btn btn-sm btn-outline-secondary" href="{{route('user.show', ['id' => $user->id])}}">Exibir</a>
			      	<a type="button" class="btn btn-sm btn-outline-secondary" href="{{route('user.edit', ['id' => $user->id])}}">Editar</a>
			      </td>
			    </tr>
			   @endforeach
			  </tbody>
			</table>
		</div>
		{{$users->links()}}
	@endif
@endsection

@push('scripts')
	<script type="text/javascript">
		$(document).ready(function()
		{
			$('#excluir').click(function()
			{
				if(confirm('Esta ação não pode ser desfeita. Excluir registros?'))
				{
					$('#users').submit();
				}
			});

			$("#toggle_all").change(function()
			{
				console.log('changed');
				if(this.checked)
				{
					console.log('checked');
					$(':checkbox[class=ids]').prop('checked', this.checked)
				}
				else
				{
					$(':checkbox[class=ids]').prop('checked', '')
				}
			})			
		});

	</script>
@endpush