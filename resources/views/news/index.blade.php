@extends('layouts.admin')
@section('title', 'Listar Notícias')

@section('toolbar')
	<a href="{{route('news.create')}}" class="btn btn-sm btn-primary"><i data-feather="plus-circle"></i> Novo</a>&nbsp;
	<button class="btn btn-sm btn-danger" id="excluir"><i data-feather="trash"></i> Excluir selecionados</button>
@endsection

@section('content')
	@if(!isset($news) || count($news) <= 0)
		@include('shared.no_results')
	@else
	<form action="{{url('news/destroy')}}" method="POST" id="users">
		@method('DELETE')
		@csrf
		<div class="table-responsive">	
			<table class="table table-striped table-sm">
			  <thead>
			    <tr>
			      <th><input type="checkbox" id="toggle_all"/></th>
			      <th>#</th>
			      <th>Titulo</th>
			      <th>Autor</th>
			      <th>Criado em</th>
			      <th>Editado em</th>
			      <th>Publicada</th>
			      <th>Ações</th>
			    </tr>
			  </thead>
			  <tbody>
			  @foreach($news as $n)
			    <tr>
			      <td><input type="checkbox" name="ids[]" class="ids" value="{{$n->id}}" /></td>
			      <td>{{$n->id}}</td>
			      <td>{{$n->title}}</td>
			      <td>{{$n->user->name}}</td>
			      <td>{{$n->created_at->format('d/m/Y \\a\\s H:i:s')}}</td>
			      <td>{{$n->updated_at->format('d/m/Y \\a\\s H:i:s')}}</td>
			      <td>{{$n->published == 1 ? 'Sim' : 'Não'}}</td>
			      <td>
			      	<a class="btn btn-sm btn-outline-secondary" href="{{route('news.show', ['id' => $n->id])}}">Exibir</a>
			      	<a type="button" class="btn btn-sm btn-outline-secondary" href="{{route('news.edit', ['id' => $n->id])}}">Editar</a>
			      </td>
			    </tr>
			   @endforeach
			  </tbody>
			</table>
		</div>
		{{$news->links()}}
	@endif
@endsection

@push('scripts')
	<script type="text/javascript">
		$(document).ready(function()
		{
			$('#excluir').click(function()
			{
				if(confirm('Esta ação não pode ser desfeita. Excluir registros?'))
				{
					$('#users').submit();
				}
			});

			$("#toggle_all").change(function()
			{
				console.log('changed');
				if(this.checked)
				{
					console.log('checked');
					$(':checkbox[class=ids]').prop('checked', this.checked)
				}
				else
				{
					$(':checkbox[class=ids]').prop('checked', '')
				}
			})			
		});

	</script>
@endpush