@extends('layouts.admin')
@section('title', $form_title)

@section('toolbar')
  <button class="btn btn-sm btn-primary" id="salvar"><i data-feather="save"></i> Salvar</button>&nbsp;
  <a href="{{route('news.index')}}" class="btn btn-sm btn-primary"><i data-feather="arrow-left"></i> Voltar</a>
@endsection

@section('content')
<div class="col-md-12">
    <form action="{{$form_route}}" method="POST" name="news_form">
      @method($form_method)
      @csrf
      <div class="row">
        <div class="col-md-12 mb-3">
          <label for="title">Titulo</label>
          <input name="title" type="text" class="form-control" placeholder="" value="{{$news->title ?? ''}}" required>          
        </div>        
      </div> 

      <div class="mb-3">
        <label for="body">Corpo da Notícia </label>        
        <textarea name="body" class="form-control text-editor" rows="5">{{$news->body ?? ''}}</textarea>        
      </div>

      <div class="checkbox">
        <input name="published" type="checkbox" value="1" @if(isset($news->published)) {{$news->published == 1 ? 'checked' : ''}} @endif>
        <label>Publicado</label>
      </div>      

      

      <input type="submit" style="display: none;" />

    </form>
  </div>
</div>
@endsection

@push('scripts')  

  <script type="text/javascript">
    $(document).ready(function()
    {
      $('#salvar').click(function()
      {        
        $('form[name=news_form]').submit();        
      });      
      
    });
  </script>

  //TinyMCE
  <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.text-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
@endpush