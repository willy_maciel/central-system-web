@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            Olá {{$user->name}}, você esta logado!
        </div>
    </div>
@endsection
