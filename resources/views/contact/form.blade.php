@extends('layouts.admin')
@section('title', $form_title)

@section('toolbar')
  <button class="btn btn-sm btn-primary" id="salvar"><i data-feather="save"></i> Salvar</button>&nbsp;
  <a href="{{route('contact.index')}}" class="btn btn-sm btn-primary"><i data-feather="arrow-left"></i> Voltar</a>
@endsection

@section('content')
<div class="col-md-12">
    <form action="{{$form_route}}" method="POST" name="contact_form">
      @method($form_method)
      @csrf
      <div class="row">
        <div class="col-md-12 mb-3">
          <label for="nome">Nome</label>
          <input name="nome" type="text" class="form-control" placeholder="" value="{{$contact->nome ?? ''}}" required>
          <div class="invalid-feedback">
            Valid first name is required.
          </div>
        </div>        
      </div>

      <div class="row">
        <div class="col-md-12 mb-3">
          <label for="telefone">Telefone</label>
          <input name="telefone" type="text" class="form-control" placeholder="" value="{{$contact->telefone ?? ''}}" required>
          <div class="invalid-feedback">
            Telefone é obrigatório
          </div>
        </div>        
      </div> 

      <div class="mb-3">
        <label for="email">Email </label>
        <input name="email" type="email" class="form-control" placeholder="you@example.com" value="{{$contact->email ?? ''}}">
        <div class="invalid-feedback">
          Please enter a valid email address for shipping updates.
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 mb-3">
          <label for="cep">CEP</label>
          <input name="cep" type="text" class="form-control" placeholder="" value="{{$contact->cep ?? ''}}" required>
          <div class="invalid-feedback">
            CEP é obrigatório
          </div>
        </div>        
      </div>

      <input type="submit" style="display: none;" />

    </form>
  </div>
</div>
@endsection

@push('scripts')
  <script type="text/javascript">
    $(document).ready(function()
    {

      //Pega elemento input do cep
      let $cep = $('input[name="cep"]');

      //Ao digitar o valor do CEP, roda a cada digito
      $cep.keyup(function()
      {
        //Pega o valor do campo CEP
        let cepVal = $cep.val();
        //Remove espaços
        cepVal = cepVal.replace(" ", "");
        //Remove -
        cepVal = cepVal.replace("-", "");
        //Remove não digitos
        cepVal = cepVal.replace(/\D/, "");
        //Seta o valor substituido no input novamente
        $cep.val(cepVal);
      });

      //Clique no botão Salvar
      $('#salvar').click(function()
      {
        //VALIDAÇÃO CEP
        //Pega o valor cep do input
        let cep = $cep.val();

        //Faz chamada para a API https://viacep.com.br/ com o cep na URL
        $.getJSON("https://viacep.com.br/ws/"+cep+"/json/", function(data)
        {
          if(data)
          {
            //Se retornou dado o CEP eh valido, envia o formulário para salvar Contato
            $('form[name=contact_form]').submit();
          }
        }).fail(function(jqxhr, textStatus, error)
        {
          //Se a API retornar falha, CEP invalido
          alert("CEP "+$cep.val()+" é Invalido.");
        });
      });

    });

  </script>
@endpush
