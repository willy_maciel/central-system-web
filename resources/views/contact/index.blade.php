@extends('layouts.admin')
@section('title', 'Listar contatos')

@section('toolbar')
	<a href="{{route('contact.create')}}" class="btn btn-sm btn-primary"><i data-feather="plus-circle"></i> Novo</a>&nbsp;
	<button class="btn btn-sm btn-danger" id="excluir"><i data-feather="trash"></i> Excluir selecionados</button>
@endsection

@section('content')
	@if(!isset($contacts) || count($contacts) <= 0)
		@include('shared.no_results')
	@else
	<form action="{{url('contact/destroy')}}" method="POST" id="contacts">
		@method('DELETE')
		@csrf
		<div class="table-responsive">	
			<table class="table table-striped table-sm">
			  <thead>
			    <tr>
			      <th><input type="checkbox" id="toggle_all"/></th>
			      <th>#</th>
			      <th>Nome</th>
			      <th>E-mail</th>
			      <th>Telefone</th>
			      <th>Cep</th>
			      <th>Criado em</th>
			      <th>Editado em</th>
			      <th>Ações</th>
			    </tr>
			  </thead>
			  <tbody>
			  @foreach($contacts as $contact)
			    <tr>
			      <td><input type="checkbox" name="ids[]" class="ids" value="{{$contact->id}}" /></td>
			      <td>{{$contact->id}}</td>
			      <td>{{$contact->nome}}</td>
			      <td>{{$contact->email}}</td>
			      <td>{{$contact->telefone}}</td>
			      <td>{{$contact->cep}}</td>
			      <td>{{$contact->created_at->format('d/m/Y \\a\\s H:i:s')}}</td>
			      <td>{{$contact->updated_at->format('d/m/Y \\a\\s H:i:s')}}</td>
			      <td>
			      	<a class="btn btn-sm btn-outline-secondary" href="{{route('contact.show', ['id' => $contact->id])}}">Exibir</a>
			      	<a type="button" class="btn btn-sm btn-outline-secondary" href="{{route('contact.edit', ['id' => $contact->id])}}">Editar</a>
			      </td>
			    </tr>
			   @endforeach
			  </tbody>
			</table>
		</div>
		{{$contacts->links()}}
	@endif
@endsection

@push('scripts')
	<script type="text/javascript">
		$(document).ready(function()
		{
			$('#excluir').click(function()
			{
				if(confirm('Esta ação não pode ser desfeita. Excluir registros?'))
				{
					$('#contacts').submit();
				}
			});

			$("#toggle_all").change(function()
			{
				console.log('changed');
				if(this.checked)
				{
					console.log('checked');
					$(':checkbox[class=ids]').prop('checked', this.checked)
				}
				else
				{
					$(':checkbox[class=ids]').prop('checked', '')
				}
			})			
		});

	</script>
@endpush
