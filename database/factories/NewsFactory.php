<?php

use Faker\Generator as Faker;

$factory->define(App\News::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(5),
        'body' 	=> $faker->paragraph,
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'published' => random_int(0, 1)
    ];
});
