# Central System - Processo Seletivo


## Descrição

1 – Desenvolver uma aplicação web em Laravel (https://laravel.com/docs/5.7) que contenha:

* Criação de CRUD de Usuários

* Criação de CRUD de Notícias

* Gerenciamento da API via Passport (https://laravel.com/docs/5.7/passport)

Obs.: Pode ser em Blade mesmo.

### Conteudo

* CRUD de usuários

* CRUD de notícias

* CRUD de Contatos com validação de CEP por ajax

* Autenticação padrão do laravel com cadastro de novos usuários pela rota /register (requerido para acesso via login)

* Modelos relacionados pelo model do Eloquent 

* Criação e edição de notícias utilizando o editor TinyMCE com upload de imagens on the fly

* Migrations e Seeds prontas (ao usar php artisan db:seed é criado 20 usuários e 20 noticias relacionadas com esses usuários)

### Instalação

* Clonar o projeto
* Rodar composer install no diretório
* Copiar o .env.example renomear para .env e configurar os dados do banco
* Rodar o comando php artisan key:generate
* Rodar o comando php artisan migrate
* Para testes, rodar o comando php artisan db:seed para gerar 20 users e 20 notícias aleatórias


## Changelog

26/06/2019 - Adicionado CRUD de contatos com validação de CEP por ajax
