<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Model Contact
use App\Contact;
//Para poder usar o Validator
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Pega todos os contatos do model Contact
        //Para usar esse model, precisamos importar o namespace no topo do código: use App\Contact;
        //Caso queira usar sem importar no topo, o namespace completo da classe deve ser especificado como no exemplo abaixo:
        //$contacts = App\Contact::paginate(10); mas para deixar o código mais limpo, e para concentar todas as dependencias dessa classe em um lugar só, é boa pratica declarar no topo com o use.
        $contacts = Contact::paginate(10);

        return view('contact.index', ['contacts' => $contacts]);

        //Segundo parametro é o array com os dados que vai jogar para a view
        //O array esta sendo declarado já no parametro para salvar espaço, nada impede de criar uma variavel antes como no exemplo abaixo:
        //$data = ['contacts' => $contacts]
        //return view('contact.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Aqui eu crio algumas variaveis para jogar para o form, para poder aproveitar o mesmo form na criação e edição de contatos
        $data = [
                'form_route'    => route('contact.store'),
                'form_method'   => 'POST',
                'form_title'    => 'Criar Contato'
                ];

        return view('contact.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validação, se validação falhar, automaticamente redireciona para o formulário com os erros
        $this->validator($request->all())->validate();

        //Cria o contato por mass assignment
        //Pega todos os inputs enviados pelo form e cria automaticamente o Contato
        //Para o Mass Assignment funcionar, tem que ser criado a propriedade $fillable ou $guarded no model especificando os campos que podem ser mass assigned
        if(Contact::create($request->all()))
        {
            $message = 'Contato Criado';  
        }
        else
        {
            $message = 'Falha ao criar contato';
        }

        return back()->with('alerts', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = Contact::FindOrFail($id);
        return view('contact.show', ['contact' => $contact]);
    }

    /**
     * Mostra o form para edição do resource
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = Contact::FindOrFail($id);

        $data = [
                'contact'          => $contact,
                'form_route'    => route('contact.update', ['id' => $contact->id]),
                'form_method'   => 'PUT',
                'form_title'    => 'Editar Contato'
                ];

        return view('contact.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = Contact::FindOrFail($id);

        $data = $request->all();        

        //Validação
        $this->validator($data)->validate();
        
        //Se a validação passar, atualizamos o contato e salvamos
        //Repare que poderiamos atualizar o contato por mass assignment (igual no metodo store) ou pelo metodo abaixo (alterando campo por campo e salvando)
        //aqui estamos fazendo desta forma para demonstrar que as 2 formas são possiveis
        $contact->nome     = $request->input('nome');
        $contact->telefone = $request->input('telefone');
        $contact->email    = $request->input('email');
        $contact->cep      = $request->input('cep');
        $contact->save();

        return back()->with('alerts', 'Contatos Atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->filled('ids'))
        {
            $ids = $request->input('ids');

            $deleted_count = Contact::destroy($ids);
            if($deleted_count > 0)
            {
                return redirect('contact')->with('alerts', $deleted_count . ' Contatos deletados');
            }
        }
        else
        {           
            return back()->with('alerts', 'Nenhum registro foi selecionado para remoção.');
        }
        
    }

    protected function validator(array $data)
    {
        //Validação dos campos do formulário
        return Validator::make($data, [
            'nome'      => ['required', 'string', 'max:255'],
            'telefone'  => ['required', 'string', 'max:255'],
            'cep'       => ['required', 'string', 'max:8'],
            'email'     => ['required', 'string', 'email', 'max:255'],
        ]);
    }
}
