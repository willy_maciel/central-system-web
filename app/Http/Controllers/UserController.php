<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $users = User::paginate(10);
        return view('user.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
                'form_route'    => route('user.store'),
                'form_method'   => 'POST',
                'form_title'    => 'Criar Usuário'
                ];

        return view('user.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //Validação
        $this->validatorCreate($request->all())->validate();
        
        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->save();

        return back()->with('alerts', 'Usuário Criado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::FindOrFail($id);
        return view('user.show', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $user = User::FindOrFail($id);

        $data = [
                'user'          => $user,
                'form_route'    => route('user.update', ['id' => $user->id]),
                'form_method'   => 'PUT',
                'form_title'    => 'Editar Usuário'
                ];

        return view('user.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::FindOrFail($id);

        $data = $request->all();

        //Se password for null remove do array para não validar
        if(is_null($data['password']))
        {
            unset($data['password']);
        }
        else
        {
            $user->password = Hash::make($request->input('password'));
        } 

        //Validação
        $this->validatorUpdate($data, $id)->validate();        
        
        $user->name = $request->input('name');
        $user->email = $request->input('email');        
        $user->save();

        return back()->with('alerts', 'Usuário Atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->filled('ids'))
        {
            $ids = $request->input('ids');

            $deleted_count = User::destroy($ids);
            if($deleted_count > 0)
            {
                return redirect('user')->with('alerts', $deleted_count . ' usuários deletados');
            }
        }
        else
        {           
            return back()->with('alerts', 'Nenhum registro foi selecionado para remoção.');
        }
        
    }

    protected function validatorUpdate(array $data, $id = null)
    {        
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($id)],
            'password' => ['sometimes', 'required', 'string', 'min:5', 'confirmed'],
        ]);
    }

    protected function validatorCreate(array $data)
    {        
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

}
