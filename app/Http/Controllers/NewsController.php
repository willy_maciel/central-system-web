<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $news = News::paginate(10);
        return view('news.index', ['news' => $news]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
                'form_route'    => route('news.store'),
                'form_method'   => 'POST',
                'form_title'    => 'Criar Notícia'
                ];

        return view('news.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //Validação
        $this->validator($request->all())->validate();
        
        $news = new News;
        $news->title = $request->input('title');
        $news->body = $request->input('body');
        $news->published = $request->input('published') === null ? 0 : 1;
        \Auth::user()->news()->save($news);        

        return back()->with('alerts', 'Notícia Criada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::FindOrFail($id);
        return view('news.show', ['news' => $news]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $news = News::FindOrFail($id);

        $data = [
                'news'          => $news,
                'form_route'    => route('news.update', ['id' => $news->id]),
                'form_method'   => 'PUT',
                'form_title'    => 'Editar Notícia'
                ];

        return view('news.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news = News::FindOrFail($id);

        $data = $request->all();
        

        //Validação
        $this->validator($data)->validate();        
        $news->title = $request->input('title');
        $news->body = $request->input('body');
        $news->published = $request->input('published') === null ? 0 : 1;
        $news->save();

        return back()->with('alerts', 'Notícia Atualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->filled('ids'))
        {
            $ids = $request->input('ids');

            $deleted_count = News::destroy($ids);
            if($deleted_count > 0)
            {
                return redirect('news')->with('alerts', $deleted_count . ' notícias deletadas');
            }
        }
        else
        {           
            return back()->with('alerts', 'Nenhum registro foi selecionado para remoção.');
        }
        
    }

    protected function validator(array $data)
    {        
        return Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'body' => ['required', 'string']            
        ]);
    }
    
}
