<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //$fillable ou $guarded são atributos do model para especificar quais campos são protegidos ou livres 
    //para gravação em mass assignment (usando Contact::create($request->all()))
    //https://laravel.com/docs/5.8/eloquent#mass-assignment
    protected $guarded = [];
}
